class StaticPagesController < ApplicationController
  
  def home
    if logged_in?
  	  @micropost = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end 
  end

  def help
  	#render text: "This is help method in Static Pages Controller."
  end

  def about
  	#render text: "This is about method in Static Pages Controller."
  end

  def contact
  	#render text: "This is about method in Static Pages Controller."
  end
end
