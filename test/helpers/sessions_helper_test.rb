require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
	
	def setup
		@user = users(:michael)
		remember(@user)
	end

	test "current_user returns right user when session is nil" do 
		#puts "AKASH	"
		#puts "@user ==> #{@user.inspect}"
		#puts "current_user ==> #{current_user.inspect}"
		assert_equal @user, current_user  
		assert is_logged_in?
	end

	test "current_user returns nil when the remember digest is wrong" do
		#puts "SHETH"
		@user.update_attribute(:remember_digest, User.digest(User.new_token))
		assert_nil current_user
	end
end